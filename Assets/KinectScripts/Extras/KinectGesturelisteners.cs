﻿using UnityEngine;
using System.Collections;

public class KinectGesturelisteners : MonoBehaviour {
    private GestureListener gestureListener;
    private GameObject Gesture;
    private static KinectGesturelisteners instance;
    public GUIText GestureInfo;
    KinectManager manager;
	// Use this for initialization

    // returns the single KinectManager instance
    public static KinectGesturelisteners Instance
    {
        get
        {
            return instance;
        }
    }

	void Start () {
        // get the gestures listener
        Gesture = GameObject.Find("KinectObject");
        gestureListener = Gesture.GetComponent<GestureListener>();
	}
	
	// Update is called once per frame
	void Update () {
        if (manager == null)
         manager = KinectManager.Instance;
        if (manager != null)
        {
            if (gestureListener.IsPSI())
                GestureInfo.guiText.text = "PSI";
            if (gestureListener.IsWave())
                GestureInfo.guiText.text = "WAVE";
            if (gestureListener.IsPush())
                GestureInfo.guiText.text = "PUSH";
        }
	}
}
