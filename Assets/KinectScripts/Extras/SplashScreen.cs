﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour {
    public float timerSplash = 0.1f;
    public GameObject DebugGUI;
    private InteractionManager manager;
    private KinectManager Kmanager;
    private GestureListener gestureListener;
    float Idle_Timer = 0.0f;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {


        if ((manager != null && manager.IsInteractionInited()) && (Kmanager != null) && (!manager.IsSceneReady()))
        {

          

            if (!manager.isPlayerDetected())
            {
                Idle_Timer += Time.deltaTime;
                //DebugGUI.guiText.text = "Idle : " + Idle_Timer.ToString();
                if (Idle_Timer > 5.0f)
                {
                    Idle_Timer = 0;
                    PlayerPrefs.SetInt("prodPlayAgain", 0); // Enables the splash screen on load
                    Application.LoadLevel(Application.loadedLevelName);
                }
            }
            else
                Idle_Timer = 0;

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                Kmanager.SensorAngle += 2;
                KinectWrapper.NuiCameraElevationSetAngle(Kmanager.SensorAngle);
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                Kmanager.SensorAngle -= 2;
                KinectWrapper.NuiCameraElevationSetAngle(Kmanager.SensorAngle);
            } 

        }
        else
        {
            if (manager == null)
              manager = InteractionManager.Instance;
            if (Kmanager == null)
              Kmanager = KinectManager.Instance;
        }
	}





}
