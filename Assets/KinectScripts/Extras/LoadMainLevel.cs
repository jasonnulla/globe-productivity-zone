using UnityEngine;
using System.Collections;

public class LoadMainLevel : MonoBehaviour 
{
	private bool levelLoaded = false;
	public GUIText GestureInfo;

	void Start()
	{
			
	}

	void Update() 
	{
		KinectManager manager = KinectManager.Instance;


		if(!levelLoaded && manager && KinectManager.IsKinectInitialized())
		{
			levelLoaded = true;
            Application.LoadLevel(Level.MECHANICS);
		}

	}
	
}
