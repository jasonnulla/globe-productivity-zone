﻿using UnityEngine;
using System.Collections;

public class productivity_drag : MonoBehaviour {
	Vector3 	oldCubePos;
	Vector3 	mouseStartPos;
	bool 		cubeClicked 	= false;
	box_bullet	bulletScript;
	
	// Use this for initialization
	void Start () {
		oldCubePos = transform.position;
		bulletScript = GameObject.FindObjectOfType <box_bullet> ();
	}
	
	// Update is called once per frame
	void Update () {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;

		if (Input.GetMouseButtonDown(0)) {
			if(collider.Raycast(ray, out hit, 100.00f)) {
				print (hit.collider.name);
				cubeClicked = true;
				mouseStartPos = Camera.main.ScreenToViewportPoint(Input.mousePosition) * 8;
			}
		} else if (Input.GetMouseButton(0) && cubeClicked == true) {
			Vector3 mousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition) * 8;
			transform.position = Vector3.MoveTowards(transform.position, oldCubePos + (mousePos - mouseStartPos), 1000);

			//Vector3 directionPos = new Vector3(-transform.position.x, transform.position.y, -transform.position.y);
			Vector3 directionPos = new Vector3(-transform.position.x, -2, -transform.position.y);
			GameObject.Find ("Sphere").transform.position = Vector3.MoveTowards(transform.position, directionPos, 1000);
			GameObject.Find ("direction_object").transform.position = Vector3.MoveTowards(transform.position, directionPos, 1000);
		} else if(Input.GetMouseButtonUp (0)) {
			float forceApplied = Mathf.Abs(oldCubePos.y - transform.position.y);

			print (Mathf.Abs(oldCubePos.x - transform.position.x));
			if(Mathf.Abs(oldCubePos.x - transform.position.x) > 1) {
				bulletScript.launchBox(forceApplied * 5f);
			} else {
				bulletScript.launchBox(forceApplied * 2.5f);
			}

			oldCubePos = transform.position;
			if(cubeClicked == true) {
				cubeClicked = false;
			}
		}

		GameObject.Find ("New Text").GetComponent<TextMesh> ().text = Camera.main.ScreenToViewportPoint (Input.mousePosition).ToString();
	}
}
