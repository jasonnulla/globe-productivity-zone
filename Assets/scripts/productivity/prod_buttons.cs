﻿using UnityEngine;
using System.Collections;

public class prod_buttons : MonoBehaviour {
	public string buttonName;

	private Ray ray;
	private RaycastHit hit;

	private Vector3 leftPoint 	= new Vector3(-18, 0, 0);
	private Vector3 centerPoint = new Vector3(0, 0, 0);
    public Vector3 defaultSize { get; set; }

	mechanics_script mechanicsScript;
    public GameObject parentObject { get; set; }

	void Awake() 
    {
		defaultSize = transform.localScale;
		mechanicsScript = FindObjectOfType<mechanics_script> ();
        collider.enabled = false;
	}

	void Start() 
    {
        gameObject.AddComponent<AudioSource>();
        audio.playOnAwake = false;
        audio.clip = Resources.Load<AudioClip>("button-select");

		//if(buttonName != "quit") {
		if(transform.parent.position.x == 18) {
			if(this.buttonName == "play" || this.buttonName == "next") {
				StartCoroutine("moveToTheLeft");
			}
		}
	    parentObject = transform.parent.gameObject;
        collider.enabled = false;

        if (transform.parent.position == centerPoint)
        {
            foreach (prod_buttons btn in transform.parent.GetComponentsInChildren<prod_buttons>())
            {
                btn.collider.enabled = true;
            }
        }
		//}
	}
	
	// Update is called once per frame
    void Update()
    {

        //if (buttonName != "quit" && collider.enabled == false && transform.parent.position == centerPoint)

		ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		if(collider.Raycast(ray, out hit, 100.00f) && this.transform.localScale != defaultSize) {
			sizeUp();
		} else {
			//shrinkDown();
		}
		
		if(Input.GetMouseButtonUp(0)) {

			if(collider.Raycast(ray, out hit, 100.00f)) {
                this.collider.enabled = false;

				if(buttonName == "play again") {
					playAgain();
				} else if(buttonName == "exit" || buttonName == "quit") {
					exitGame();
				} else if(buttonName == "next") {
					nextSlide();
				} else if(buttonName == "skip") {
					skipMechanics();
                } else if(buttonName == "play") {
                    playGame();
				} else if(buttonName == "game over") {
					showScore();
				}

			}
		}
	}

    public void playAgain()
    {
        PlaySound();
		PlayerPrefs.SetInt("prodPlayAgain", 1); // Disables the splash screen on load
		Application.LoadLevel(Level.MECHANICS);
	}

    public void exitGame()
    {
        PlaySound();
        GameObject.FindObjectOfType<SceneController> ().ExitGame();

        //PlayerPrefs.SetInt("prodPlayAgain", 0); // Enables the splash screen on load
        //Application.LoadLevel(Application.loadedLevelName);
	}

    public void nextSlide()
    {
        PlaySound();
        mechanicsScript.nextPage(mechanicsScript.currentPage() + 1);
        StartCoroutine("nextPage");
	}

    public void skipMechanics()
    {
        PlaySound();
        mechanicsScript.nextPage(4);
        StartCoroutine("nextPage");
	}

    public void playGame() 
    {
        PlaySound();
        mechanicsScript.nextPage(5);
        StartCoroutine("nextPage");
    }

    void PlaySound()
    {
        audio.Play();
    }

    public int getCurrentPage()
    {
        return mechanicsScript.currentPage();
    }
	public void showScore() {
		FindObjectOfType<tagline_script> ().moveLeft();
	}

	public void sizeUp() {
        if (this.buttonName == "next" || this.buttonName == "skip" || this.buttonName == "play")
        {
			gameObject.transform.localScale = new Vector3(2.5f, 2.5f, defaultSize.z);
        } else if(buttonName == "play again" || buttonName == "exit") {
            gameObject.transform.localScale = new Vector3(3.25f, 3.25f, defaultSize.z);
        } else if(buttonName == "game over") {
            gameObject.transform.localScale = new Vector3(2.5f, 2.5f, defaultSize.z);
        }
	}

	public void shrinkDown() {
		if(transform.localScale != defaultSize)
			transform.localScale = defaultSize;
	}

	IEnumerator nextPage() {
        while (transform.parent.position != leftPoint)
        {
            transform.parent.position = Vector3.Lerp(transform.parent.position, leftPoint, 0.1f + Time.deltaTime);

            if ((transform.parent.position - leftPoint).magnitude < 0.05f)
                transform.parent.position = leftPoint;

            yield return null;
        }

        Destroy(transform.parent.gameObject);
		yield return null;
	}

	IEnumerator moveToTheLeft() {
		while(transform.parent.position != centerPoint) {
			transform.parent.position = Vector3.Lerp(transform.parent.position, centerPoint, 0.1f + Time.deltaTime);

            if ((transform.parent.position - centerPoint).magnitude < 0.05f)
            {
                transform.parent.position = centerPoint;
                foreach (prod_buttons btn in transform.parent.GetComponentsInChildren<prod_buttons>())
                {
                    btn.collider.enabled = true;
                }
            }
			
			yield return null;
		}

        foreach (prod_buttons btn in transform.parent.GetComponentsInChildren<prod_buttons>())
        {
            btn.transform.parent = null;
        }
		yield return null;
	}
}
